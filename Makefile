.POSIX:

PREFIX = /usr/local
BINDIR  = $(PREFIX)/bin
MANDIR  = $(PREFIX)/share/man

EXE = go-deblob
BLOBS = test/fixtures/hello test/fixtures/hello.a
CFLAGS = -Os -s

all: $(EXE)

go-deblob: *.go go.mod go.sum
	go build -mod=`ls -d vendor 2> /dev/null` -o $@

.PHONY: clean
clean:
	rm -f $(BLOBS) $(EXE)

test/fixtures/hello.a: test/fixtures/hello
	$(AR) -rc $@ $<

.PHONY: test
test: test/fixtures/hello test/fixtures/hello.a
	cp -r test/fixtures test/checkDir-fixtures
	go test
	rm -fr test/checkDir-fixtures

.PHONY: install
install:
	mkdir -p $(DESTDIR)$(BINDIR)
	cp -p $(EXE) $(DESTDIR)$(BINDIR)/
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp -p $(EXE).1 $(DESTDIR)$(MANDIR)/man1/
