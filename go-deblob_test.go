// This file is part of https://hacktivis.me/git/go-deblob
// Copyright © 2019 Haelwenn (lanodan) Monnier <contact+go-deblob-notice@hacktivis.me>
// SPDX-License-Identifier: BSD-3-Clause
package main

import (
	"testing"

	"os"
	"path/filepath"
	"reflect"
)

func TestIsBlob(t *testing.T) {
	tests := []struct {
		path   string
		expect bool
	}{
		{"test/fixtures/hello.1", false},
		{"test/fixtures/hello.c", false},
		{"test/fixtures/hello", true},
		// {"test/fixtures/hello.o", true},
		{"test/fixtures/hello.a", true},
		{"test/fixtures/empty", false},
		// {"test/fixtures/option.rom", true},
	}

	for _, test := range tests {
		got := isBlob(test.path)
		if got != test.expect {
			t.Errorf("isBlob(%s) was incorrect, got: %v, expects: %v.", test.path, got, test.expect)
		}
	}
}

func TestCheckDir(t *testing.T) {
	dirname := "test/checkDir-fixtures"
	var files_before []string
	var files_after []string

	err := filepath.Walk(dirname, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			t.Errorf("filepath.Walk(%s, /*files_before*/): %s\n", dirname, err)
			return err
		}
		files_before = append(files_before, path)
		return nil
	})
	if err != nil {
		t.Errorf("filepath.Walk(%s, /*files_before*/): %s\n", dirname, err)
		return
	}

	checkDir(dirname, false)

	err = filepath.Walk(dirname, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			t.Errorf("filepath.Walk(%s, /*files_after*/): %s\n", dirname, err)
			return err
		}
		files_after = append(files_after, path)
		return nil
	})
	if err != nil {
		t.Errorf("filepath.Walk(%s, /*files_after*/): %s\n", dirname, err)
		return
	}
	if reflect.DeepEqual(files_before, files_after) {
		t.Errorf("Expected files_before and files_after to differ. files_after: %v\n", files_after)
	}
}
